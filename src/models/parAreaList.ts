export class ParAreaList{
  constructor(
      public number?: number,
      public  parareaNo? : string,
      public  parareaName?  : string,
      public  reference?  : string,
      public  activeStatus? : boolean,
      public  facilityName?  : string,
      public  glcode?  : string
  ){}
}
export interface PageInfo {
  currentPageNumber: number;
  pageSize: number;
  listLength: number;
  totalPages: number;
}
export interface PaginatorHelper {
  parAreaList: ParAreaList[];
  pageInfo: PageInfo;
}
