import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { ParAreaListComponent } from './par-area-list/par-area-list.component';
import { HomeComponent } from './home/home.component';
import { ReactiveFormsModule , FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    ParAreaListComponent,
    HomeComponent 
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule ,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      { path: 'app-par-area-list', component:ParAreaListComponent }
    ])
  ],
  providers: [],
  bootstrap: [AppComponent,ParAreaListComponent]
})
export class AppModule { }
