import { Component, OnInit } from '@angular/core';
import { ParAreaList,PaginatorHelper} from '../../models/parAreaList';
import {ParAreaListDataService} from '../../data.services/parAreaList.data.service';

@Component({
  selector: 'app-par-area-list',
  templateUrl: './par-area-list.component.html',
  styleUrls: ['./par-area-list.component.scss'],
  providers:[ParAreaListDataService]
})

export class ParAreaListComponent implements OnInit {
  pageShows:number[];
  paginatorHelp:PaginatorHelper;
  parAreas=[];
  tableMode = true;
  lastPage:number;
  currentPage=1;
  countPage:number;
  countItems:number;
  constructor(private parAreaListService:ParAreaListDataService ) {  }

    ngOnInit(){
      this.loadParAreasPages(this.currentPage);
    }  

    loadParAreas() {
      this.parAreaListService.getParAreas().subscribe(
        (data) => { this.parAreas = data as ParAreaList[]
     });
    } 

    loadParAreasPages(pSize:number){
     this.tableMode = true;
       this.parAreaListService.getParAreasByPage(pSize).subscribe(
         (data) => { this.paginatorHelp= data as PaginatorHelper;
           this.lastPage=this.paginatorHelp.pageInfo.totalPages;
           if (this.paginatorHelp.pageInfo.currentPageNumber<this.lastPage-1){
              this.currentPage=this.paginatorHelp.pageInfo.currentPageNumber;
           }
           this.countItems=this.paginatorHelp.pageInfo.listLength;
           this.parAreas=this.paginatorHelp.parAreaList;
      });     
    }

    setDataForPaging(current:number){
     var b= document.getElementsByTagName("a");
     var i =0;
     for(i=0; i<b.length; i++){
      b[i].className="nonActive";
      }
     document.activeElement.className="active";
    }

    NextPage(){
      if(this.currentPage<this.lastPage){
      this.currentPage+=1;
      }
    }

    PreviousPage(){
      if(this.currentPage>1){
        this.currentPage-=1;
      }
    }

   /* calculateLastPage(qualityOnPage:number){
      this.lastPage=parseInt((this.parAreas.length/qualityOnPage).toString(),10)+1;
    }*/
}

