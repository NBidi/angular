import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ParAreaListComponent } from './par-area-list.component';
describe('ParAreaListComponent', () => {
  let component: ParAreaListComponent;
  let fixture: ComponentFixture<ParAreaListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParAreaListComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParAreaListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
