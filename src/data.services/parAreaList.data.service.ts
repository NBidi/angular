import { Injectable } from '@angular/core';
import { ParAreaList } from "../models/parAreaList";
import { HttpClient} from '@angular/common/http';
@Injectable()
export class ParAreaListDataService {
  private url = "http://localhost:61985/api/ParArea";
  constructor(private http: HttpClient) {
  }
  getParAreas() {
    return this.http.get(this.url);
  }
  createParArea(pararea: ParAreaList) {
    return this.http.post(this.url, ParAreaList);
  }
  getParAreasByPage(id:number) {
    return this.http.get(this.url+ "/" + id);
  }
}